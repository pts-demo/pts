# Ansible Playbooks

Scripts to provision the hardware instrastructure necessary to run PTS. 

## Overview

PTS hardware infrastructure is composed of two main elements:
- __Control Node__: This is the orchestration system and the gateway for GitLab CI to communicate with all the SUTs. 
- __System Under Test__: The system to run the PTS workloads.


```plantuml
@startuml component
actor user
node gitlab as "GitLab"
node pts as "PTS Hardware Infrastructure" {
component control as "Control\n Node"
component sut1 as "System under\ntest (SUT) #1"
component sut2 as "System under\ntest (SUT) #2"
}

user -> gitlab
gitlab -> control
control <--> sut1: ssh
control <--> sut2: ssh
@enduml 
``` 

## Playbooks

| Playbook                                     | Description                                                            |
|----------------------------------------------|------------------------------------------------------------------------|
| [pts-control.yml](pts-control.yml)           | Provisions a CS9 system to be a Control Node                           |
| [pts-sut.yml](pts-sut.yml)                   | Provisions a CS9 system to be a System Under Test.                     |
| [pts-register-sut.yml](pts-register-sut.yml) | Registers a System Under Test to a Control Node so PTS can utilize it. |


## [pts-sut.yml](pts-sut.yml)

Ansible playbook to provision the system under test for PTS.

### Optional Variables

Optional variables to change the way the SUT setup is done.

#### benchmark-wrapper

Following variables are useful during development.

- `bw_fork`: Allows specifying the name of the fork of benchmark-wrapper on GitHub, by default it is `cloudbulldozer`.
- `bw_ref`: Allows specifying a git ref of benchmark-wrapper. Could be a branch/tag/etc. Defaults to `master`.
- `bw_venv_path`: Specifies where to store the Python virutal environment. Defaults to `~/bw_venv`.

#### sysbench

`sysbench_version` specifies the tagged version of sysbench to use. Could be any git ref.


### Tags

Tags to change what is run in the playbook. If none are specified, all of these are run.

#### Primary tags

- `pts`: Installs everything needed to run PTS on a SUT. This __does not include any workloads__.
- `sysbench`: Builds and installs [sysbench](https://github.com/akopytov/sysbench).
- `coremark-pro`: Installs dependencies required for CoreMark-Pro and does a test build.

#### Additional tags for development

- `benchmark-wrapper`: Installs only [benchmark-wrapper](https://github.com/cloud-bulldozer/benchmark-wrapper). Using `pts` tag will automatically include this.
### Example usage

These commands are run from the root of the git directory. 

#### Addressing a host without inventory file

Executes the playbook on two systems with named `raspberry-pi` and `nvidia-agx`
with the username of `root`. 

```bash
ansible-playbook \
-u root \
--private-key=/path/private/key/id_rsa  \
-i raspberry-pi,nividia-agx \
ansible/pts-sut.yml
```

#### Installing a different version of benchmark-wrapper

Installs benchmark-wrapper on the `raspberyy-pi` SUT from 
`github.com/ewchong/benchmark-wrapper` using the branch `new-feature`.

```bash
ansible-playbook \
-u root \
--private-key=/path/private/key/id_rsa  \
--extra-vars 'bw_fork=ewchong bw_ref=new-feature' \
-i raspyberry-pi, \
ansible/pts-sut.yml
```

#### Installing a different version of sysbench

Installs the latest sysbench on the `master` branch.

```bash
# Installs the latest sysbench on the `master` branch.
ansible-playbook \
-u root \
--private-key=/path/private/key/id_rsa  \
--extra-vars 'sysbench_version=master' \
-i raspyberry-pi, \
ansible/pts-sut.yml
```

#### Using tags to install only a subset of functionality

```bash

# Install only PTS and no other workloads on a SUT named `raspberry-pi`
ansible-playbook \
-u root \
--private-key=/path/private/key/id_rsa  \
-i raspberry-pi \
--tags pts
ansible/pts-sut.yml

# Install sysbench and CoreMark-Pro only on a SUT named `raspberry-pi`
ansible-playbook \
-u root \
--private-key=/path/private/key/id_rsa  \
-i raspberry-pi \
--tags "sysbench, coremark-pro"
ansible/pts-sut.yml

```

## [pts-control.yml](pts-control.yml)

Install all the necessary dependenceis needed for the control
node. Assumes CentOS 9 Stream.

### Example usage

These commands are run from the root of the git directory. 

#### Addressing a host without inventory file

Executes the playbook on a system called `my-control-node` with the username 
of `root`. 

```bash
ansible-playbook \
  -u root \
  --private-key=/path/private/key/id_rsa  \
  -i my-control-node, \
  ansible/pts-control.yml
```

## [pts-register-sut](pts-register-sut)

Registers a System Under Test (SUT) to a Control Node.

### Required Variables

| Variable              | Description                                                                                                                                                                    |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `sut_name`            | A friendly name of the host                                                                                                                                                    |
| `sut_hostname`        | Hostname or IP address of the SUT                                                                                                                                              |
| `gitlab_runner_tags`  | Tags to associate with the SUT                                                                                                                                                 |
| `gitlab_runner_token` | Token to register the runner with a GitLab project. Currently found [here](https://gitlab.com/groups/redhat/edge/tests/pts-demo/-/settings/ci_cd) under the `Runners` section. |

#### GitLab runner tags

The tags on the GitLab runner is used by PTS to select the correct runner for
the job. All PTS runners require the `PTS` tag which is automatically prepended
when using the Ansible script.

The rest are split into the following categories with what is currently used:

1. __Location__: Which control node can access this SUT.
    - `PERF40`
    - `JOHNLAB`
    - `EDLAB`

2. __Type of Platform__: Identifies the HW of the SUT.
    - `PI4`
    - `AGX`
    - `AWS`

3. __Operating System__: Operating system the SUT runs.
    - `RHEL8x`
    - `UBUNTU`
    - `CENTOS8`
    - `RHIVOS`

### Optional Variables

Optional variables that are geared towards defaults used by Automotive Perf&Scale team.

| Variable          | Description                                      | Default value
|-------------------|--------------------------------------------------|---------------------------|
| `sut_user`        | SSH username for SUT                             | `root`                    |
| `sut_port`        | SSH port for SUT                                 | `22`                      |
| `sut_private_key` | Path to SSH private key to use to connect to SUT | `~/.ssh/id_rsa_auto_perf`
| `sut_strict_host` | Configures SSH' `StrictHostChecking`.            | `no`                      |

### Example

Registers a SUT named `pi-test` without specifying any optional variables:
- to a control node with a hostname of `perf40`
- at address `192.168.1.10`
- with tags:
    - `PI4`: Running on a Raspberry PI4
    - `PERF40`: Specifies that it is Running on `perf40` control node.
    - `CENTOS9`: Has CentOS 9 Stream installed

```bash
ansible-playbook \
  -i perf40, \
  --private-key=/path/private/key/id_rsa  \
  --extra-vars='{
    "sut_name": "pi-test",
    "sut_hostname": "192.168.1.10",
    "gitlab_runner_token": "GITLAB_REGISTRATION_TOKEN",
    "gitlab_runner_tags": "PI4,CENTOS9,PERF40"
    }' \
  ansible/pts-register-sut.yml
```

To verify, see if the runner is listed [here](https://gitlab.com/groups/redhat/edge/tests/pts-demo/-/settings/ci_cd) under the `Runners` section.
