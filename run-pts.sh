#!/usr/bin/env bash
usage() {
	echo "$0 <host> <test username> [-c] <tool name> <added benchmark wrapper options>" 1>&2
	echo "Enviorment variables:"
	echo "- BRANCH: sets branch to clone from"
	echo "- DEV: Copies the local direcotry instead of cloning"
	echo "- FOLDER_NAME: Name of folder to put output"
	exit 1;
}


[ -z "$es" ] && printf "Error: The 'es' enviornment variable not set. Specify a Elasticsearch server. Example:\n\n\t export es=http://es-server:9200\n\n" && exit 1
if [ "$#" -lt 3 ]; then
  usage
fi
# TODO Verify ansible exists

# TODO Doesn't quite work yet
WORKDIR="~"

HOST=$1
shift
#clustername=$2
test_user=$1
echo "Test user: $test_user"
shift
container_flag=$1
shift
tool=$1
shift
container=$(echo $tool | tr -d '-')
BENCHMARK_WRAPPER_ARGS=$@

# Derives the SUT based on the description assigned to the runner
echo "Running $tool on $HOST..."


# Set up pts and output artifact directory
if [[ -z "$DEV" ]]; then
  [[ -z "$BRANCH" ]] && BRANCH=main;
  echo "Cloning PTS..."
  ssh $HOST "git clone --branch $BRANCH https://gitlab.com/redhat/edge/tests/pts-demo/pts.git; mkdir _output"
else
  if ! ssh $HOST "command -v rsync &> /dev/null"; then
    ssh $HOST "dnf install rsync -y"
  fi
  # Set up pts and output artifact directory
  echo "Copying PTS..."
  ssh $HOST "mkdir _output pts"
  rsync -avth . $HOST:$WORKDIR/pts/
fi

#####
# Hack to generate a conf file for sysbench
#####
if [ $tool = "sysbench" ]
then
  echo "Generating a config.conf file for sysbench arugments..."
  echo "$BENCHMARK_WRAPPER_ARGS" | tr ' ' '\n' > config.conf
  cat config.conf
  BENCHMARK_WRAPPER_ARGS="-f config.conf"
  scp config.conf $HOST:$WORKDIR/config.conf 
else
  rm config.conf > /dev/null 2>&1
fi

hostname=$(ssh $HOST /usr/bin/hostnamectl hostname)
# Generate cluster name before the colon
echo "Running benchmark on $hostname..."

# BUG: Fails because it runs workload.sh inside a container and that means PCP odens't work.  So for now it is probably only baremetal
ssh $HOST ". \$BW_VENV_PATH/bin/activate; cd $WORKDIR; test_user=$test_user clustername=$hostname pts/workload.sh $container_flag $tool $BENCHMARK_WRAPPER_ARGS"
ssh $HOST "mv *.log *.env *.archive _pcp *.conf _output"
scp -r $HOST:$WORKDIR/_output .

# Clean up
echo "Cleaning up..."
ssh $HOST "rm -rf _output pts"

source ./_output/pts-run.env
echo "Appending system config..."
./system-config.py $HOST -f _output/${PTS_FILENAME}.archive

echo "Submitting results to Elasticserach server: ${es}..."
./push-results.sh $PWD/_output/${PTS_FILENAME}.archive

if [ $tool = "sysbench" ]
then
  index="e0c71c80-95b4-11ec-9cc6-b529dd3f37d9"
elif [ $tool = "coremark-pro" ] 
then
  index="341d9510-7ebf-11ec-9cc6-b529dd3f37d9"
else
  exit 0
fi
# Assumes port number for kibana is on 5601
[ -z "$KIBANA_URL" ] && KIBANA_URL=$(echo $es | sed 's/9200/5601/g')

RESULT_URL="$KIBANA_URL/app/kibana#/discover?_g=(refreshInterval:(pause:!t,value:0),time:(from:'$PTS_START_TIME',to:'$PTS_END_TIME'))&_a=(columns:!(_source),index:'$index',interval:auto,query:(language:kuery,query:''),sort:!(date,desc))"
echo "$RESULT_URL" >> _output/${PTS_FILENAME}.log
printf "View results here: \n\n%s, $RESULT_URL"

mv _output $FOLDER_NAME/$PTS_FILENAME
