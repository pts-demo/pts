#!/usr/bin/env python3
import requests
import os
import json
import sys
import configargparse
import boto3
from botocore.client import Config
from botocore import UNSIGNED
import botocore.exceptions



def parse_args():

    parser = configargparse.ArgumentParser()
    parser.add_argument(
            '-a',
            '--archive-file',
            help='benchmark-wrapper archive file',
            required=True
            )
    parser.add_argument(
            '-p',
            '--pcp-json',
            help='JSON file output from pcp2json',
            dest="json_file",
            required=False
            )
    parser.add_argument(
            '-r',
            '--ref',
            help='A git ref of PTS to use such as branch, tag or commit hash.\
                    Defaults to `main`. Mainly for PTS development purposes.',
            default='main',
            required=False
            )
    parser.add_argument(
            '-t',
            '--token',
            help='A GitLab API token to access PTS',
            env_var="GITLAB_TOKEN",
            dest="gitlab_token",
            )
    parser.add_argument(
            '-s',
            '--s3-url', help='A S3 URL to upload files to',
            env_var="PTS_S3_BUCKET_URL",
            required=True
            )
    parser.add_argument(
            '-b',
            '--bucket-name', help='Name of the S3 bucket',
            env_var="PTS_S3_BUCKET_NAME",
            required=True
            )
    parser.add_argument(
            '-N',
            '--no-credentials', help='S3 bucket does not require credentials',
            action='store_true',
            required=False
            )

    return parser.parse_args()


def s3_upload(s3, key, file, url, bucket):
    s3.Bucket(bucket).upload_file(
            file,
            os.path.basename(file),
            ExtraArgs={'ACL': 'public-read'})

    return {"variables": [
                {"key": key,
                    "value": f"{url}/{bucket}/{file}"}
                ]
            }


def upload_files(session, args, config):
    s3 = session.resource('s3', endpoint_url=args.s3_url, config=config)

    data = s3_upload(s3,
                     "ARCHIVE_FILE",
                     args.archive_file,
                     args.s3_url,
                     args.bucket_name)

    if args.json_file:
        s3_result = s3_upload(s3,
                              "PCP_JSON",
                              args.json_file,
                              args.s3_url,
                              args.bucket_name)
        data.update(s3_result)
    return data


def main():
    args = parse_args()
    args.s3_url = args.s3_url.rstrip('/')

    headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'PRIVATE-TOKEN': args.gitlab_token
            }

    params = (
            ('ref', args.ref),
            )

    auth_config = Config(signature_version=UNSIGNED) if args.no_credentials\
        else None
    print("Uploading...")
    data = upload_files(boto3, args, auth_config)

    print("Upload successful!")
    req = requests.post(
            'https://gitlab.com/api/v4/projects/26947957/pipeline',
            headers=headers,
            params=params,
            data=json.dumps(data)
            ).json()

    if 'web_url' in req:
        print("PTS triggered successfully. Check on pipeline here:")
        print("\n\t", req["web_url"], "\n")
    else:
        print("PTS trigger failed:\n\n", req)

if __name__ == "__main__":
    sys.exit(main())
